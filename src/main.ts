import { AppComponent } from './app/app.component';
import { bootstrapApplication } from '@angular/platform-browser';
import { provideRouter } from '@angular/router';
import { routes } from './app/routes';
import { contactRoutes } from './app/contact/routes';
import { UserServiceConfig } from './app/greeting/user.service';

bootstrapApplication(AppComponent, {
  providers: [
    provideRouter([...routes, ...contactRoutes]),
    { provide: UserServiceConfig, useValue: { userName: 'Miss Marple' } },
  ],
}).catch(err => console.error(err));
