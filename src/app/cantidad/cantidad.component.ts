import { Component, Input } from '@angular/core';

import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-cantidad',
  standalone: true,
  imports: [],
  template: `
    <div class="flex items-center gap-4">
      <button class="m-0" (click)="onRemove()" color="primary">-</button>
      <div>{{ quantity }}</div>
      <button class="m-0" (click)="onAdd()" color="primary">+</button>
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: CantidadComponent,
    },
  ],
})
export class CantidadComponent implements ControlValueAccessor {
  quantity = 0;

  @Input() increment = 1;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange = (quantity: number) => {};

  onTouched = () => {};

  touched = false;
  disabled = false;

  onAdd() {
    this.markAsTouched();
    if (!this.disabled) {
      this.quantity += this.increment;
      this.onChange(this.quantity);
    }
  }

  onRemove() {
    this.markAsTouched();
    if (!this.disabled) {
      this.quantity -= this.increment;
      this.onChange(this.quantity);
    }
  }

  writeValue(quantity: number): void {
    this.quantity = quantity;
  }

  registerOnChange(onChange: (quantity: number) => void): void {
    this.onChange = onChange;
    console.log('function registerOnChange registered');
  }

  registerOnTouched(onTouched: () => void): void {
    this.onTouched = onTouched;
    console.log('function onTouched registered');
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
