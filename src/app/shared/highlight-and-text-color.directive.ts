import { Directive } from '@angular/core';
import { HighlightDirective } from './highlight.directive';
import { TextColorDirective } from './text-color.directive';

@Directive({
  selector: '[appHighlightAndTextColor]',
  standalone: true,
  hostDirectives: [HighlightDirective, TextColorDirective],
})
export class HighlightAndTextColorDirective {
  constructor() {
    // Do Nothing
  }
}
