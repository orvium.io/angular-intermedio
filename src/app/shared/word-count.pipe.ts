import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wordCount',
  standalone: true,
})
export class WordCountPipe implements PipeTransform {
  transform(value: string): number {
    const wordCount = value ? value.split(/\s+/) : 0;
    return wordCount ? wordCount.length : 0;
  }
}
