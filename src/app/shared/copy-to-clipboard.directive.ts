import { Directive, HostListener, Input } from '@angular/core';
import { Clipboard } from '@angular/cdk/clipboard';

@Directive({
  selector: '[appCopyToClipboard]',
  standalone: true,
})
export class CopyToClipboardDirective {
  @Input('appCopyToClipboard') text = '';

  constructor(public clipboard: Clipboard) {}

  @HostListener('click') copyToClipboard(): void {
    if (this.clipboard.copy(this.text)) {
      console.log('Copy to clipboard succesfully!');
    } else {
      console.log('Copy to clipboard failed!');
    }
  }
}
