import { Injectable, OnDestroy } from '@angular/core';

import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

export class Customer {
  constructor(
    public id: number,
    public name: string,
    public description: string,
    public petName?: string,
  ) {}
}

const CUSTOMERS: Customer[] = [
  new Customer(1, 'Julian', 'Mi nombre es Julian y tengo 2 perros y una casa en la playa', 'Drogo'),
  new Customer(
    2,
    'Eric',
    'Mi nombre es Eric y me gusta hacer descripciones extramadamente largas para que no salgan de manera correcta cuando salen en las aplicaciones en las que me registro',
  ),
];

const FETCH_LATENCY = 500;

/** Simulate a data service that retrieves heroes from a server */
@Injectable({ providedIn: 'root' })
export class CustomersService implements OnDestroy {
  constructor() {
    console.log('CustomersService instance created.');
  }
  ngOnDestroy() {
    console.log('CustomersService instance destroyed.');
  }

  getCustomers(): Observable<Customer[]> {
    return of(CUSTOMERS).pipe(delay(FETCH_LATENCY));
  }

  getCustomer(id: number | string): Observable<Customer> {
    const customer$ = of(CUSTOMERS.find(customer => customer.id === +id)!);
    return customer$.pipe(delay(FETCH_LATENCY));
  }
}
