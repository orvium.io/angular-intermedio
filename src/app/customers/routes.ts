import { Routes } from '@angular/router';
import { CustomersDetailComponent } from './customers-detail.component';
import { CustomersListComponent } from './customers-list.component';
import { CustomersComponent } from './customers.component';

export const customerRoutes: Routes = [
  {
    path: '',
    component: CustomersComponent,
    children: [
      { path: '', component: CustomersListComponent },
      { path: ':id', component: CustomersDetailComponent },
    ],
  },
];
