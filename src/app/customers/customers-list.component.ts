import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { Customer, CustomersService } from './customers.service';
import { RouterLink } from '@angular/router';
import { AsyncPipe } from '@angular/common';
import { HighlightDirective } from '../shared/highlight.directive';

@Component({
  template: `
    <h3 appHighlight>Customer List</h3>
    @for (customer of customers | async; track customer) {
      <div>
        <a routerLink="{{ customer.id }}">{{ customer.id }} - {{ customer.name }}</a>
      </div>
    }
  `,
  standalone: true,
  imports: [HighlightDirective, RouterLink, AsyncPipe],
})
export class CustomersListComponent {
  customers: Observable<Customer[]>;
  constructor(private customersService: CustomersService) {
    this.customers = this.customersService.getCustomers();
  }
}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/
