import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';

import { Customer, CustomersService } from './customers.service';
import { FormsModule } from '@angular/forms';
import { NgTemplateOutlet } from '@angular/common';
import { HighlightDirective } from '../shared/highlight.directive';
import { ShortenPipe } from '../shared/shorten.pipe';
import { WordCountPipe } from '../shared/word-count.pipe';
import { CopyToClipboardDirective } from '../shared/copy-to-clipboard.directive';
import { TextColorDirective } from '../shared/text-color.directive';
import { HighlightAndTextColorDirective } from '../shared/highlight-and-text-color.directive';
import { AlertComponent } from '../shared/alert/alert.component';

@Component({
  template: `
    <h3 appHighlightAndTextColor>Customer Detail</h3>

    <app-alert class="p-2">
      <span info>*Extra information</span>
      You are watching the customer number {{ customer.id }}
    </app-alert>

    @if (customer) {
      <div class="border-solid border-2 border-gray-400 max-w-xl m-2">
        <span class="p-2">Id: {{ customer.id }}</span>
        <br />
        <label class="p-2"
          >Name:
          <input [(ngModel)]="customer.name" />
        </label>
        <p class="p-2">
          Description: {{ customer.description | shorten: 55 }}
          <button
            class="rounded-full bg-blue-500 text-white cursor-pointer"
            aria-label="Copy"
            type="button"
            [appCopyToClipboard]="customer.description"
            >Copy
          </button>
          @if (customer.petName) {
            <ng-container *ngTemplateOutlet="PetName"></ng-container>
          }
          @if (!customer.petName) {
            <ng-container *ngTemplateOutlet="NoPet"></ng-container>
          }
        </p>
        <span class="flex justify-end">({{ customer.description | wordCount }})</span>
      </div>
    }
    <br />
    <a routerLink="../">Customer List</a>

    <ng-template #PetName>
      <p class="p-2">Pet name: {{ customer.petName }}</p>
    </ng-template>

    <ng-template #NoPet>
      <p class="p-2">This customer doesn't own any pet yet</p>
    </ng-template>
  `,
  standalone: true,
  imports: [
    HighlightDirective,
    FormsModule,
    RouterLink,
    ShortenPipe,
    WordCountPipe,
    CopyToClipboardDirective,
    TextColorDirective,
    HighlightAndTextColorDirective,
    NgTemplateOutlet,
    AlertComponent,
  ],
})
export class CustomersDetailComponent implements OnInit {
  customer!: Customer;

  constructor(
    private route: ActivatedRoute,
    private customersService: CustomersService,
  ) {}

  ngOnInit() {
    const id = parseInt(this.route.snapshot.paramMap.get('id')!, 10);
    this.customersService.getCustomer(id).subscribe(customer => (this.customer = customer));
  }
}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/
