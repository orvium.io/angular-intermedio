// Exact copy except import UserService from greeting
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormsModule,
  ReactiveFormsModule,
  FormControl,
  FormArray,
  ValidatorFn,
  ValidationErrors,
  AbstractControl,
} from '@angular/forms';

import { UserService } from '../greeting/user.service';

import { Contact, ContactService } from './contact.service';
import { AwesomePipe } from '../shared/awesome.pipe';
import { HighlightDirective } from '../shared/highlight.directive';

import { AsyncValidatorFn } from '@angular/forms';
import { Observable, delay, map, of } from 'rxjs';
import { CantidadComponent } from '../cantidad/cantidad.component';

interface ContactForm {
  name: FormControl<string>;
  age: FormControl<number>;
  birthDate: FormControl<Date>;
  address: FormGroup<AddressForm>;
  tlfs: FormArray<FormControl<string>>;
}

interface AddressForm {
  street: FormControl<string>;
  city: FormControl<string>;
  state: FormControl<string>;
  zip: FormControl<string>;
}

export function validateDate(datelimit: Date): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const selectedDate = new Date(control.getRawValue());
    if (selectedDate === null) {
      return null;
    }
    return selectedDate.getTime() > datelimit.getTime() ? { invalidDate: true } : null;
  };
}

export function validateBirthdateMatchAge(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const form = control as FormGroup<ContactForm>;
    const birthDate = new Date(form.controls.birthDate.value);
    const age = Number(form.controls.age.value);
    const birthDateAge = new Date().getFullYear() - birthDate.getFullYear();

    return birthDateAge !== age ? { ageInvalid: true } : null;
  };
}

export function validateExistingName(): AsyncValidatorFn {
  return (control: AbstractControl): Observable<ValidationErrors | null> => {
    const name = control.getRawValue();

    const observable = of(name).pipe(
      delay(500),
      map((name: string) => {
        return name === 'Antonio' ? { nameExists: true } : null;
      }),
    );
    return observable;
  };
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule, HighlightDirective, AwesomePipe, CantidadComponent],
})
export class ContactComponent implements OnInit {
  contact!: Contact;
  contacts: Contact[] = [];

  msg = 'Loading contacts ...';
  userName = '';

  contactForm = new FormGroup<ContactForm>(
    {
      name: new FormControl('', {
        nonNullable: true,
        validators: [Validators.required],
        asyncValidators: [validateExistingName()],
      }),
      age: new FormControl(
        { value: 0, disabled: false },
        {
          nonNullable: true,
          validators: [Validators.required, Validators.min(18), Validators.max(65)],
        },
      ),
      birthDate: new FormControl(new Date(), {
        nonNullable: true,
        validators: [validateDate(new Date())],
      }),
      address: new FormGroup<AddressForm>({
        street: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
        city: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
        state: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
        zip: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
      }),
      tlfs: new FormArray<FormControl<string>>([]),
    },
    {
      validators: [validateBirthdateMatchAge()],
    },
  );

  // contactForm = this.fb.nonNullable.group({
  //   name: ['', Validators.required],
  //   age: [0, Validators.required],
  //   address: this.fb.nonNullable.group({
  //     street: ['', Validators.required],
  //     city: ['', Validators.required],
  //     state: ['', Validators.required],
  //     zip: ['', Validators.required],
  //   }),
  //   tlfs: this.fb.nonNullable.array<string[]>([]),
  // });

  constructor(
    private contactService: ContactService,
    userService: UserService,
    private formBuilder: FormBuilder,
  ) {
    this.userName = userService.userName;
  }

  ngOnInit() {
    console.log('value', this.contactForm.value);
    console.log('getRawValue', this.contactForm.getRawValue());
    this.setupForm();
  }

  setupForm() {
    this.contactService.getContacts().subscribe(contacts => {
      this.msg = '';
      this.contacts = contacts;
      this.contact = contacts[0];
      this.contactForm.controls.name.setValue(this.contact.name);
    });
  }

  next() {
    let ix = 1 + this.contacts.indexOf(this.contact);
    if (ix >= this.contacts.length) {
      ix = 0;
    }
    this.contact = this.contacts[ix];
    console.log(this.contacts[ix]);
  }

  onSubmit() {
    const newName = this.contactForm.controls.name.value;
    this.displayMessage('Saved ' + newName);
    this.contact.name = newName;
  }

  newContact() {
    this.displayMessage('New contact');
    this.contactForm.controls.name.setValue('');
    this.contact = { id: 42, name: '' };
    this.contacts.push(this.contact);
  }

  /** Display a message briefly, then remove it. */
  displayMessage(msg: string) {
    this.msg = msg;
    setTimeout(() => (this.msg = ''), 1500);
  }

  showFormValues() {
    console.log(this.contactForm.getRawValue());
  }
}
