import { Routes } from '@angular/router';
import { ItemsListComponent } from './items-list.component';
import { ItemsDetailComponent } from './items-detail.component';

export const itemRoutes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: ItemsListComponent },
  { path: ':id', component: ItemsDetailComponent },
];
