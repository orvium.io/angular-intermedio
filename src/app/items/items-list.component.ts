import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { Item, ItemService } from './items.service';
import { RouterLink } from '@angular/router';
import { AsyncPipe } from '@angular/common';

@Component({
  template: `
    <h3 appHighlight>Items List</h3>
    @for (item of items | async; track item) {
      <div>
        <a routerLink="{{ '../' + item.id }}">{{ item.id }} - {{ item.name }}</a>
      </div>
    }
  `,
  standalone: true,
  imports: [RouterLink, AsyncPipe],
})
export class ItemsListComponent {
  items: Observable<Item[]>;

  constructor(private itemService: ItemService) {
    this.items = this.itemService.getItems();
  }
}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/
