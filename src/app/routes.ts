import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: '', redirectTo: 'contact', pathMatch: 'full' },
  { path: 'items', loadChildren: () => import('./items/routes').then(m => m.itemRoutes) },
  {
    path: 'customers',
    loadChildren: () => import('./customers/routes').then(m => m.customerRoutes),
  },
];
